var fs = require('fs')
var conf = JSON.parse(fs.readFileSync('./srv-config.json'))
var promise = require('bluebird')
var pgp = require('pg-promise')({ promiseLib: promise })

//PostgreSQL

var pgConnectionData = {
  host: conf.database.host,
  port: conf.database.port,
  database: conf.database.database,
  user: conf.database.user,
  password: conf.database.password,
}

console.log(pgConnectionData)

var pgDatabase = pgp(pgConnectionData)

module.exports = pgDatabase
