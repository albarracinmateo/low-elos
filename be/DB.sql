
CREATE TABLE IF NOT EXISTS public.clips (
    clip serial NOT NULL,
    url_clip character varying NOT NULL,
    game_id numeric NOT NULL,
    author numeric NOT NULL,
    title character varying,
    votes numeric
);


CREATE TABLE IF NOT EXISTS public.games (
    game_id numeric NOT NULL,
    game_name character varying
);

CREATE TABLE IF NOT EXISTS public.users (
    author serial NOT NULL,
    username character varying NOT NULL,
    pass character varying NOT NULL
);


CREATE INDEX IF NOT EXISTS fki_fk_clips ON public.clips USING btree (clip, url_clip);


DO $$
BEGIN 
    IF NOT EXISTS(SELECT 1 FROM pg_constraint WHERE conname='pk_clips') THEN
        ALTER TABLE clips ADD CONSTRAINT pk_clips
            PRIMARY KEY (clip, url_clip);
    END IF;
    IF NOT EXISTS(SELECT 1 FROM pg_constraint WHERE conname='pk_games') THEN
        ALTER TABLE games ADD CONSTRAINT pk_games 
            PRIMARY KEY (game_id);
    END IF;
    IF NOT EXISTS(SELECT 1 FROM pg_constraint WHERE conname='pk_users') THEN
        ALTER TABLE users ADD CONSTRAINT pk_users 
            PRIMARY KEY (author);
    END IF;
    
    -- IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'fk_games') THEN
    --     ALTER TABLE games ADD CONSTRAINT fk_games
    --         FOREIGN KEY (game_id) REFERENCES public.clips(game_id) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;
    -- END IF;
    -- IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'fk_users') THEN
    --     ALTER TABLE users ADD CONSTRAINT fk_users
    --         FOREIGN KEY (author) REFERENCES public.clips(author) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;
    -- END IF;
    
END;
$$;

ALTER TABLE public.games ADD COLUMN IF NOT EXISTS last_game boolean;
ALTER TABLE public.clips ADD COLUMN IF NOT EXISTS thumbnail character varying;
