var express = require('express')
var router = express.Router({ mergeParams: true })
var pgDatabase = require('../DB')
const path = '/users/'
var fs = require('fs')
var conf = JSON.parse(fs.readFileSync('./srv-config.json'))

router.post(path, (req, res) => {
  var username = req.body.username
  var password = req.body.password

  var initialQuery = "SELECT * FROM users WHERE username = '" + username + "';"
  console.log(initialQuery)
  pgDatabase.query(initialQuery, req.body).then((initialData) => {
    if (initialData.length != 0) {
      let error = 'No se pudo crear la cuenta, el usuario ya existe.'
      console.log(error)
      return res.status(400).json({ error: error })
    }
    var createQuery =
      "INSERT INTO users (username, pass) VALUES ('" +
      username +
      "', '" +
      password +
      "');"
    console.log(createQuery)
    pgDatabase.query(createQuery, req.body).then((createData) => {
      let response = 'El usuario se creo exitosamente!'
      console.log(response)
      return res.status(200).json({ response: response })
    })
  })
})

router.get(path, (req, res) => {
  var username = req.query.username
  var password = req.query.password

  var usernameQuery = "SELECT * FROM users WHERE username = '" + username + "';"
  console.log(usernameQuery)
  pgDatabase.query(usernameQuery, req.query).then((usernameData) => {
    if (usernameData.length == 0) {
      let error = 'Usuario no registrado.'
      console.log(error)
      return res.status(400).json({ error: error })
    }

    var passQuery =
      "SELECT * FROM users WHERE username = '" +
      username +
      "' AND pass = '" +
      password +
      "';"
    console.log(passQuery)
    pgDatabase.query(passQuery, req.query).then((passData) => {
      if (passData.length == 0) {
        let error = 'Contraseña incorrecta.'
        console.log(error)
        return res.status(400).json({ error: error })
      }
      var author = passData[0]
      console.log(author)
      return res.status(200).json({ response: passData[0] })
    })
  })
})

module.exports = router
