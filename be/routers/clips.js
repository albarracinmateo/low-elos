var express = require('express')
var router = express.Router({ mergeParams: true })
var pgDatabase = require('../DB')
const path = '/clips/'
var fs = require('fs')
var conf = JSON.parse(fs.readFileSync('./srv-config.json'))

router.post(path, (req, res) => {
  var url = req.body.url
  var game = req.body.game
  var author = req.body.author
  var title = req.body.title
  var thumbnail = req.body.thumbnail

  var initialQuery =
    "SELECT * FROM clips WHERE url_clip = '" +
    url +
    "' AND game_id = " +
    game +
    ';'
  console.log(initialQuery)

  pgDatabase.query(initialQuery, req.body).then((initialData) => {
    if (initialData != 0) {
      let error = 'El clip ya existe.'
      console.log(error)
      return res.status(400).json({ error: error })
    }
    var insertClip =
      "INSERT INTO clips (url_clip, game_id, author, title, thumbnail) VALUES ('" +
      url +
      "', " +
      game +
      ', ' +
      author +
      ", '" +
      title +
      "', '" +
      thumbnail +
      "');"
    console.log(insertClip)
    pgDatabase.query(insertClip, req.body).then((insertData) => {
      let response = 'Se inserto el clip exitosamente!'
      return res.status(200).json({ response: response })
    })
  })
})

router.get(path, (req, res) => {
  var game = req.query.game

  if (game == 0) {
    var zeroQuery = 'SELECT * FROM games WHERE last_game = true;'
    console.log(zeroQuery)

    pgDatabase.query(zeroQuery, req.query).then((zeroData) => {
      console.log(zeroData)
      var gameID = zeroData[0].game_id

      var gameZeroClips = 'SELECT * FROM clips WHERE game_id = ' + gameID + ';'
      console.log(gameZeroClips)

      pgDatabase.query(gameZeroClips, req.query).then((clipsData) => {
        console.log(clipsData)
        return res.status(200).send(clipsData)
      })
    })
  } else {
    var initialQuery = 'SELECT * FROM clips WHERE game_id = ' + game + ';'
    console.log(initialQuery)

    pgDatabase.query(initialQuery, req.query).then((initialData) => {
      return res.status(200).send(initialData)
    })
  }
})

module.exports = router
