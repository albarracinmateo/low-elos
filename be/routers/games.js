var express = require('express')
var router = express.Router({ mergeParams: true })
var pgDatabase = require('../DB')
const path = '/games/'
var fs = require('fs')
var conf = JSON.parse(fs.readFileSync('./srv-config.json'))

router.post(path, (req, res) => {
  var game = req.body.game
  if (game != 0) {
    var initialQuery = 'UPDATE games SET last_game = false;'

    console.log(initialQuery)

    pgDatabase.query(initialQuery, req.query).then(() => {
      var updateQuery =
        'UPDATE games SET last_game = true WHERE game_id = ' + game + ';'
      console.log(updateQuery)

      pgDatabase.query(updateQuery, req.query).then(() => {
        return res.status(200).json({ response: 'Se actualizo exitosamente' })
      })
    })
  } else {
    return res.status(200).json({ response: 'No se actualizo' })
  }
})

router.get(path, (req, res) => {
  var game = req.query.game
  if (game == 0) {
    var zeroQuery = 'SELECT * FROM games WHERE last_game = true;'
    console.log(zeroQuery)

    pgDatabase.query(zeroQuery, req.query).then((zeroData) => {
      console.log(zeroData)
      return res.status(200).send(zeroData)
    })
  } else {
    var initialQuery = 'SELECT * FROM games WHERE game_id = ' + game + ';'
    console.log(initialQuery)

    pgDatabase.query(initialQuery, req.query).then((initialData) => {
      return res.status(200).send(initialData)
    })
  }
})

module.exports = router
