var express = require('express')
var http = require('http')
var https = require('https')
var app = express()
var fs = require('fs')
var bodyParser = require('body-parser')
var pjson = require('./package.json')
var conf = JSON.parse(fs.readFileSync('./srv-config.json'))
var dataBase = fs.readFileSync('./DB.sql').toString()

const cors = require('cors')
const pgDatabase = require('./DB')
const HTTP_PORT = conf.puerto

var clips = require('./routers/clips')
var users = require('./routers/users')
var games = require('./routers/games')

let port = process.env.PORT || HTTP_PORT

app.set('port', process.env.PORT || HTTP_PORT)
app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
)
app.use(cors())
app.use(bodyParser.json())
app.use(clips)
app.use(users)
app.use(games)


pgDatabase.query(dataBase, function (err, result) {
  done()
  if (err) {
    console.log('error: ', err)
    process.exit(1)
  }
  process.exit(0)
})

function startHttpServer() {
  // var server = false;
  if (conf.ssl == true) {
    var privateKey = fs.readFileSync(conf.key, 'utf8')
    var certificate = fs.readFileSync(conf.crt, 'utf8')

    var credentials = { key: privateKey, cert: certificate }
    var server = https.createServer(credentials, app)
    console.log('Seguridad Aplicada!')
    func.pathLog(contador + ' ' + 'Servidor con Seguridad Aplicada!')
  } else {
    var server = http.createServer(app)
  }

  server.listen(port, function () {
    console.log('\nEl servidor esta corriendo en el puerto: ' + port + '\n')
  })

  return server
}

express.response.badRequest = function (args) {
  this.writeContinue()
  this.statusCode = 400
  this.send(args)
  this.end()
}

express.response.sqlError = function (args) {
  this.writeContinue()
  this.statusCode = 503
  this.send(args)
  this.end()
}

httpServer = startHttpServer()
