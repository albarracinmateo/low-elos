import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { RouterModule } from '@angular/router'
import { ROUTES } from './app-routing'
import { AppComponent } from './app.component'
import { HomeComponent } from './home/home.component'
import { GamesComponent } from './home/home-content/clips/games/games.component'
import { InfoComponent } from './info/info.component'
import { HomeContentComponent } from './home/home-content/home-content.component'
import { TopbarComponent } from './topbar/topbar.component'
import { ClipsComponent } from './home/home-content/clips/clips.component'
import { HttpClientModule } from '@angular/common/http'
import { ModalComponent } from './modal/modal.component'
import { ClipsModalComponent } from './modal/modals/clips-modal/clips-modal.component'
import { DefaultModalComponent } from './modal/modals/default-modal/default-modal.component'
import { LoginModalComponent } from './modal/modals/login-modal/login-modal.component'
import { RegisterModalComponent } from './modal/modals/register-modal/register-modal.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserComponent } from './home/home-content/user/user.component';
import { UploadModalComponent } from './modal/modals/upload-modal/upload-modal.component';
import { ActionsComponent } from './home/home-content/actions/actions.component'

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GamesComponent,
    InfoComponent,
    HomeContentComponent,
    TopbarComponent,
    ClipsComponent,
    ModalComponent,
    ClipsModalComponent,
    DefaultModalComponent,
    LoginModalComponent,
    RegisterModalComponent,
    UserComponent,
    UploadModalComponent,
    ActionsComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
