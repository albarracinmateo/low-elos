import { Component, Input } from '@angular/core'

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css'],
})
export class InfoComponent {
  @Input() peopleInfo: string
  @Input() btnPeople: string

  constructor() {
    this.peopleInfo = 'none'
    this.btnPeople = 'flex'
  }

  displayPeople() {
    this.peopleInfo = 'flex'
    this.btnPeople = 'none'
  }
}
