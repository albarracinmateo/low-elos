import { Component, Inject, Input } from '@angular/core'
import { Route, Router } from '@angular/router'
import { AppService } from '../app-service'
import { ModalService } from '../modal/modal-service'

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css'],
})
export class TopbarComponent {
  @Input() userMenu: string
  constructor(
    private router: Router,
    @Inject(ModalService) private modal: ModalService,
    @Inject(AppService) private serv: AppService,
  ) {
    this.userMenu = 'none'
    this.loginOk = false
    this.serv.returnUser().subscribe((data) => {
      this.loginOk = this.serv.returnLoginOk()
      this.username = data[1]
    })
  }

  goToHome() {
    this.router.navigate(['/home'])
  }

  userEvent() {
    if (!this.serv.returnLoginOk()) {
      this.modal.open('login-modal')
    } else {
      if (this.userMenu == 'flex') {
        this.userMenu = 'none'
      } else {
        this.userMenu = 'flex'
      }
    }
  }

  goToUser() {
    this.router.navigate(['/home/user'])
  }

  logout() {
    this.serv.logoutUser()
    this.userMenu = 'none'
  }

  username: any
  loginOk: boolean
}
