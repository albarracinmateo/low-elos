import { Inject, Injectable } from '@angular/core'
import { HttpClient, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs/internal/Observable'
import { BehaviorSubject, tap } from 'rxjs'
import { ModalService } from './modal/modal-service'
const datosJSON = require('./app-config.json')

@Injectable({
  providedIn: 'root',
})
export class AppService {
  constructor(
    private httpClient: HttpClient,
    @Inject(ModalService) private modal: ModalService,
  ) {
    this.environment = { endpoint: '' }
    this.environmentUrl = new URL('http://localhost:8080')
    this.colores = []
    this.game = 0
    this.url = new BehaviorSubject<string>('UNDEFINED')
    this.loginOk = false
    this.user = new BehaviorSubject<any[]>([undefined])
  }

  initConfig = new Observable<void>((sus) => {
    console.log(datosJSON)

    this.environment = {
      endpoint: datosJSON.endpoint,
    }
    this.environmentUrl = new URL(datosJSON.endpoint)

    this.colores = datosJSON.colores
    var rootElement = document.documentElement
    var primerColor = this.colores.primerColor
    var segundoColor = this.colores.segundoColor
    var tercerColor = this.colores.tercerColor
    var textoColor = this.colores.textoColor

    rootElement.style.setProperty('--primerColor', primerColor)
    rootElement.style.setProperty('--segundoColor', segundoColor)
    rootElement.style.setProperty('--tercerColor', tercerColor)
    rootElement.style.setProperty('--textoColor', textoColor)
  })

  initMaster() {
    return this.initConfig.toPromise()
  }

  getGenerico(params: HttpParams, ruta: string) {
    return this.httpClient.get(this.environmentUrl.toString() + ruta, {
      headers: { 'Cache-Control': 'max-age=10800' },
      params,
    })
  }
  postGenerico(url: string, body: object) {
    return this.httpClient.post(this.environmentUrl.toString() + url, body)
  }

  getJSON() {
    return datosJSON
  }

  createUser(username: string, password: string) {
    return this.postGenerico('users', {
      username: username,
      password: password,
    }).pipe(tap((data) => {}))
  }

  loginUser(username: string, password: string) {
    let httpParams = new HttpParams()
    return this.getGenerico(
      httpParams,
      'users?username=' + username + '&password=' + password,
    ).pipe(tap((data) => {}))
  }

  logoutUser() {
    localStorage.removeItem('loginOk')
    localStorage.removeItem('username')
    localStorage.removeItem('password')
    this.loginOk = false
    this.user = new BehaviorSubject<any[]>([undefined])
    location.reload()
    this.modal.closeAll()
  }

  setLoginOk(logued: boolean) {
    this.loginOk = logued
  }

  returnLoginOk() {
    return this.loginOk
  }

  setUser(author: any, username: any) {
    let userArray = []
    userArray.push(author, username)
    this.user.next(userArray)
  }

  returnUser() {
    return this.user.asObservable()
  }

  setClip(
    url: string,
    game: number,
    title: string,
    author: number,
    thumbnail: string,
  ) {
    return this.postGenerico('clips', {
      url: url,
      game: game,
      title: title,
      author: author,
      thumbnail: thumbnail,
    }).pipe(tap((data) => {}))
  }

  getClips(game: number) {
    let httpParams = new HttpParams()
    return this.getGenerico(httpParams, 'clips?game=' + game).pipe(
      tap((data) => {}),
    )
  }

  getGames(game: number) {
    let httpParams = new HttpParams()
    return this.getGenerico(httpParams, 'games?game=' + game).pipe(
      tap((data) => {}),
    )
  }

  setLastGame(game: number) {
    return this.postGenerico('games', { game: game }).subscribe(() => {})
  }

  setGame(game: number) {
    this.getGames(game)
    this.game = game
  }

  returnGame() {
    return this.game
  }

  setUrl(url: string) {
    this.url.next(url)
  }

  returnUrl(): Observable<string> {
    return this.url.asObservable()
  }

  private environment: { endpoint: string }
  private environmentUrl: URL
  private game: number
  private colores: any
  private url: BehaviorSubject<string>
  private loginOk!: boolean
  private user!: BehaviorSubject<any[]>
}
