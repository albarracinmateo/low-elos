import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ActionsComponent } from './home/home-content/actions/actions.component';
import { ClipsComponent } from './home/home-content/clips/clips.component';
import { GamesComponent } from './home/home-content/clips/games/games.component';
import { UserComponent } from './home/home-content/user/user.component';

export const ROUTES: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    children: [
      { path: '', component: ActionsComponent },
      { path: 'clips', component: GamesComponent },
      { path: 'clip', component: ClipsComponent },
      { path: 'user', component: UserComponent },
    ],
  },
  { path: '**', redirectTo: 'home' },
];
