import { Component, Inject, OnInit } from '@angular/core';
import { AppService } from './app-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'lowElosAwards';

  constructor(@Inject(AppService) private serv: AppService) {

  }
  ngOnInit() {
    this.serv.initMaster();
  }
}
