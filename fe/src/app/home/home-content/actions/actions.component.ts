import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/app-service';

@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.css'],
})
export class ActionsComponent {
  constructor(
    private router: Router,
    @Inject(AppService) private serv: AppService
  ) {}

  goToClips() {
    this.router.navigate(['/home/clips']);
  }
  goToTematicas() {
    this.router.navigate(['/home/tematicas']);
  }
}
