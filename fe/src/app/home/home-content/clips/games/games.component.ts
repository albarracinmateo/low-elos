import { Component, Inject } from '@angular/core'
import { Router } from '@angular/router'
import { AppService } from 'src/app/app-service'

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css'],
})
export class GamesComponent {
  constructor(
    private router: Router,
    @Inject(AppService) private serv: AppService,
  ) {}

  goToGame(game: any) {
    this.serv.setGame(game)
    this.serv.setLastGame(game)
    this.router.navigate(['/home/clip'])
  }
}
