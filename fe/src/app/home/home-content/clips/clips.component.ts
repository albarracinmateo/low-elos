import { Component, Inject, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { AppService } from 'src/app/app-service'
import { ModalService } from 'src/app/modal/modal-service'

@Component({
  selector: 'app-clips',
  templateUrl: './clips.component.html',
  styleUrls: ['./clips.component.css'],
})
export class ClipsComponent implements OnInit {
  constructor(
    @Inject(AppService) private serv: AppService,
    @Inject(ModalService) private modal: ModalService,
    private sanitizer: DomSanitizer,
  ) {
    this.juego = this.serv.returnGame()
    this.clips = []
    this.gameName = ''
  }

  ngOnInit() {
    this.serv.getGames(this.juego).subscribe((game) => {
      let gameArray: any[] = []
      gameArray.push(game)
      gameArray = gameArray[0]
      // console.log(gameArray)

      if (this.gameName == undefined || this.gameName == '') {
        this.gameName = gameArray[0].game_name
      }
    })
    this.serv.getClips(this.juego).subscribe((clip) => {
      this.clips.push(clip)
      this.clips = this.clips[0]
      for (let i = 0; i < this.clips.length; i++) {
        const element = this.clips[i]
        let url = element.url_clip
        url = url.split('/')
        this.imgClip = url[url.length - 1]
      }
    })
  }

  containsObject(obj: any, list: any) {
    var i
    for (i = 0; i < list.length; i++) {
      if (list[i].src === obj.src) {
        return true
      }
    }

    return false
  }

  openClip(url: string) {
    this.serv.returnUrl().subscribe((data) => {
      this.urlData = data
    })
    this.serv.setUrl(url)
    this.modal.open('clips-modal')
    // if (this.urlData != url) {
    //   this.serv.setUrl(url)
    //   this.modal.open('clips-modal')
    // } else {
    //   this.modal.open('clips-modal')
    // }
  }

  uploadClip() {
    if (this.serv.returnLoginOk()) {
      this.modal.open('upload-modal')
    } else {
      this.modal.open('login-modal')
    }
  }

  juego: number
  clips: any
  gameName: string
  urlData: any
  imgClip: any
}
