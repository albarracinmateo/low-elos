import { Injectable } from '@angular/core'
import { ModalComponent } from './modal.component'

@Injectable({
  providedIn: 'root',
})
export class ModalService {
  constructor() {
    this.modals = []
  }

  closeAll() {
    if (this.modals.length > 0) {
      this.modals.forEach((k) => {
        if (k.id != 'modal-error') {
          k.close()
        }
      })
    }
  }

  add(modal: any) {
    this.modals.push(modal)
  }

  remove(id: string) {
    this.modals = this.modals.filter((x) => x.id !== id)
  }

  async open(id: string) {
    var searchModal: ModalComponent | undefined = undefined
    // console.log(this.modals);

    for (let modal of await this.modals) {
      if (modal.id === id) {
        searchModal = modal
      }
    }
    if (searchModal != undefined && searchModal.isOpen == false) {
      searchModal.open()
    }
  }

  close(id: string) {
    if (this.modals.length > 0) {
      this.modals.forEach((k) => {
        if (k.id === id) {
          k.close()
        }
      })
    }
  }

  private modals: ModalComponent[]
}
