import {
  Component,
  ElementRef,
  Inject,
  Input,
  OnInit,
  Renderer2,
} from '@angular/core'
import { ModalService } from './modal-service'

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css'],
})
export class ModalComponent implements OnInit {
  @Input()
  id!: string
  constructor(
    @Inject(ModalService) private modalService: ModalService,
    private el: ElementRef,
    private render: Renderer2,
  ) {
    this.openModal = false
  }
  ngOnInit(): void {
    if (!this.id) {
      return
    }
    
    this.render.addClass(this.el.nativeElement, 'notvisible')
    this.render.appendChild(document.body, this.el.nativeElement)
    this.modalService.add(this)
  }
  ngOnDestroy(): void {
    this.remove()
  }
  remove() {
    this.modalService.remove(this.id)
    var parentnode = this.render.parentNode(this.el.nativeElement)
    this.render.removeChild(parentnode, this.el.nativeElement)
  }
  open(): void {
    let bodyPage = document.querySelector('body')
    if (bodyPage != null) {
      bodyPage.style.cssText = 'overflow: hidden;'
    }

    this.render.addClass(this.el.nativeElement, 'visible')
    this.openModal = true
  }
  close(): void {
    
    let bodyPage = document.querySelector('body')
    if (bodyPage != null) {
      bodyPage.style.cssText = ''
    }

    this.render.removeClass(this.el.nativeElement, 'visible')
    this.openModal = false
  }
  get isOpen() {
    return this.openModal
  }
  private openModal: boolean
}
