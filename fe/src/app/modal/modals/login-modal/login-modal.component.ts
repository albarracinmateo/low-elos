import { Component, Inject, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { AppService } from 'src/app/app-service'
import { ModalService } from '../../modal-service'

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.css'],
})
export class LoginModalComponent implements OnInit {
  constructor(
    @Inject(AppService) private serv: AppService,
    @Inject(ModalService) private modal: ModalService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    if (
      localStorage.getItem('loginOk') == 'true' &&
      localStorage.getItem('username') != 'undefined'
    ) {
      this.serv
        .loginUser(
          localStorage.getItem('username')!,
          localStorage.getItem('password')!,
        )
        .subscribe((data: any) => {
          this.loginOk = true
          this.username = data.response.username
          this.password = data.response.pass

          localStorage.setItem('loginOk', this.loginOk.toString())
          localStorage.setItem('username', this.username)
          localStorage.setItem('password', this.password)

          this.serv.setUser(data.response.author, this.username)
          this.serv.setLoginOk(this.loginOk)
          this.modal.closeAll()
        })
    }
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    })
  }
  createUser() {
    this.modal.close('login-modal')
    this.modal.open('register-modal')
    // this.modal.close('login-modal')
  }

  submitForm() {
    this.serv
      .loginUser(
        this.formFields?.['username'].value,
        this.formFields?.['password'].value,
      )
      .subscribe(
        (data: any) => {
          this.loginOk = true
          this.username = data.response.username
          this.password = data.response.pass

          localStorage.setItem('loginOk', this.loginOk.toString())
          localStorage.setItem('username', this.username)
          localStorage.setItem('password', this.password)

          this.serv.setUser(data.response.author, this.username)
          this.serv.setLoginOk(this.loginOk)
          this.modal.closeAll()
        },
        (err) => {  
          this.loginForm.setErrors({ incorrect: true })
          this.errorGen = err['error']['error']
          this.loginOk = false
        },
      )
  }

  get formFields() {
    return this.loginForm.controls
  }

  loginForm!: FormGroup
  errorGen!: string
  loginOk!: boolean
  username!: string
  password!: string
}
