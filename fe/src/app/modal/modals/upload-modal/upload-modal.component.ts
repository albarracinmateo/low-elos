import { Component, Inject } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { AppService } from 'src/app/app-service'
import { ModalService } from '../../modal-service'

@Component({
  selector: 'app-upload-modal',
  templateUrl: './upload-modal.component.html',
  styleUrls: ['./upload-modal.component.css'],
})
export class UploadModalComponent {
  constructor(
    @Inject(AppService) private serv: AppService,
    @Inject(ModalService) private modal: ModalService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.uploadForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      url: ['', [Validators.required]],
    })
    this.serv.returnUser().subscribe((data: any) => {
      if (data != undefined) {
        this.author = data[0]
      }
    })
  }

  uploadClip() {
    let url = this.formFields?.['url'].value
    url = url.split('/')

    let idYoutube = url[url.length - 1]
    idYoutube = idYoutube.split('?v=')
    idYoutube = idYoutube[idYoutube.length - 1]

    url = url[0] + '//' + url[2] + '/embed/' + idYoutube
    console.log(url)

    let thumbail =
      'http://img.youtube.com/vi/' + idYoutube + '/maxresdefault.jpg'

    this.serv.getGames(this.serv.returnGame()).subscribe((game) => {
      let gameArray: any[] = []
      gameArray.push(game)
      gameArray = gameArray[0]
      this.game = gameArray[0].game_id

      this.serv
        .setClip(
          url,
          parseInt(this.game),
          this.formFields?.['title'].value,
          this.author,
          thumbail,
        )
        .subscribe(
          () => {
            this.modal.close('upload-modal')
            location.reload()
          },
          (err) => {
            this.uploadForm.setErrors({ incorrect: true })
            this.errorGen = err['error']['error']
          },
        )
    })
  }

  get formFields() {
    return this.uploadForm.controls
  }

  uploadForm!: FormGroup
  errorGen!: string
  loginOk!: boolean
  title!: string
  url!: string
  author!: number
  game: any
}
