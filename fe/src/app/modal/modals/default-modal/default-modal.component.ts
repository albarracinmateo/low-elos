import { Component, Inject } from '@angular/core'
import { AppService } from 'src/app/app-service'
import { ModalService } from '../../modal-service'

@Component({
  selector: 'app-default-modal',
  templateUrl: './default-modal.component.html',
  styleUrls: ['./default-modal.component.css'],
})
export class DefaultModalComponent {
  constructor(@Inject(ModalService) private modal: ModalService) {}
  close() {
    this.modal.closeAll()
  }
}
