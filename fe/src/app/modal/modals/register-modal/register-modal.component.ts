import { Component, Inject } from '@angular/core'
import { FormBuilder, Validators, FormGroup } from '@angular/forms'
import { AppService } from 'src/app/app-service'
import { ModalService } from '../../modal-service'

@Component({
  selector: 'app-register-modal',
  templateUrl: './register-modal.component.html',
  styleUrls: ['./register-modal.component.css'],
})
export class RegisterModalComponent {
  constructor(
    @Inject(AppService) private serv: AppService,
    @Inject(ModalService) private modal: ModalService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    })
  }

  createUser() {
    this.serv
      .createUser(
        this.formFields?.['username'].value,
        this.formFields?.['password'].value,
      )
      .subscribe(
        () => {
          this.modal.close('register-modal')
          this.modal.open('login-modal')
        },
        (err) => {
          this.registerForm.setErrors({ incorrect: true })
          this.errorGen = err['error']['error']
        },
      )
  }

  get formFields() {
    return this.registerForm.controls
  }

  registerForm!: FormGroup
  errorGen!: string
  loginOk!: boolean
  username!: string
  password!: string
}
