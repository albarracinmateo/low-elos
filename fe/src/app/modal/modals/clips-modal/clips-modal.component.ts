import { Component, Inject, Input, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { Observable } from 'rxjs'
import { AppService } from 'src/app/app-service'

@Component({
  selector: 'app-clips-modal',
  templateUrl: './clips-modal.component.html',
  styleUrls: ['./clips-modal.component.css'],
})
export class ClipsModalComponent implements OnInit {
  @Input() iFrame: any
  @Input() loader: any

  constructor(
    @Inject(AppService) private serv: AppService,
    private sanitizer: DomSanitizer,
  ) {
    this.getUrl = this.serv.returnUrl()
    this.url = 'UNDEFINED'
    this.iFrame = this.iFrame || 'block'
    this.iFrame = this.iFrame || 'none'

  }

  ngOnInit(): void {
    
    this.getUrl.subscribe((data) => {
      if (this.url != data) {
        this.iFrame = 'none'
        if (this.iFrame == 'none') {
          this.loader = 'contents'
        }
        setTimeout(() => {
          this.loader = 'none'
          this.iFrame = 'block'
        }, 700)
      }

      this.url = this.sanitizer.bypassSecurityTrustResourceUrl(data)
    })
  }

  getUrl: Observable<string>
  url: any
}
