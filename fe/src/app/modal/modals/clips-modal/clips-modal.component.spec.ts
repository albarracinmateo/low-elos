import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClipsModalComponent } from './clips-modal.component';

describe('ClipsModalComponent', () => {
  let component: ClipsModalComponent;
  let fixture: ComponentFixture<ClipsModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClipsModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ClipsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
